module.exports = {
  publicPath: process.env.NODE_ENV === 'production'
    ? '/~cs62160144/learn_bootstrap/'
    : '/'
}
